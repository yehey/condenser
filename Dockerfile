FROM lopsided/archlinux

MAINTAINER jacob@blurt.foundation

COPY . /condenser

WORKDIR /condenser

RUN pacman -Syyu --noconfirm yarn node-gyp npm git base-devel

RUN mkdir tmp && \
    yarn install && \
    yarn build

ENV PORT 8080
ENV NODE_ENV production

EXPOSE 8080

CMD [ "yarn", "run", "start" ]


